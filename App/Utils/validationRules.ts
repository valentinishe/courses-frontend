   
export function isRequired(value){
    return Boolean(value);
}

export function isLength(min, max) {
    return (value) => !Boolean((String(value).length < min || String(value).length > max) && String(value).length !== 0);
}

export function isName(value) {
    const regularName = /^[a-zA-ZА-Яа-я]+$/g;
    return regularName.test(value);
}

export function isText(value) {
    const regularName = /^[a-zA-ZА-Яа-я _-0-9]+$/g;
    return regularName.test(value);
}
