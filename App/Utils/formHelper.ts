interface DataFormType {
  name: string;
  value: any;
}

export function changeForm({ name, value }: DataFormType): void {
  this.setState(state => ({
    ...state,
    form: {
      ...state.form,
      [name]: {
        value,
        error: false
      }
    }
  }));
}

export function changeFormMulty(
  data: { DataFormType },
  callback?: Function
): void {
  const state = { ...this.state };

  if (state.form) {
    Object.keys(state.form).forEach(key => {
      if (data.hasOwnProperty(key)) {
        state.form[key].value = data[key];
      }
    });
  }

  this.setState({ ...state }, callback);
}
