import { ModelUserType } from "Types/Models/User";

// export function getRole(role: ModelUserType["position"]): string {
//   return ;
// }

export const ROLES = {
  USER: 0,
  MENTOR: 1,
  ADMIN: 2
};

export function isAdmin(role: ModelUserType["position"]): boolean {
  return ROLES.ADMIN === +role;
}

export function isMentor(role: ModelUserType["position"]): boolean {
  return ROLES.MENTOR === +role;
}

export function isUser(role: ModelUserType["position"]): boolean {
  return ROLES.USER === +role;
}

export function isDashboardLevel(role: ModelUserType["position"]): boolean {
  return ROLES.ADMIN === +role || ROLES.MENTOR === +role;
}
