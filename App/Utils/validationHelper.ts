import { has, get } from "lodash";

export function validationFields(rules, cb) {
  const form = this.state.form;
  let isExistError = false;

  Object.keys(form).forEach(field => {
    if (has(rules, field)) {
      const fieldRules = get(rules, field);
      Object.keys(fieldRules.Methods).forEach(r => {
        if (form[field].error) {
          isExistError = true;
          return;
        }
        if (!fieldRules.Methods[r](form[field].value)) {
          form[field].error = fieldRules.Messages[r];
        }
      });
    }
  });

  this.setState(
    {
      ...this.state,
      form
    },
    () => {
      if (!isExistError) {
        cb();
      }
    }
  );
}

export function errorToForm(errors) {
  const form = this.state.form;
  if (!errors) {
    return;
  }
  Object.keys(errors).forEach(field => {
    form[field] = {
      ...form[field],
      error: errors[field]
    };
  });

  this.setState({
    ...this.state,
    form
  });
}
