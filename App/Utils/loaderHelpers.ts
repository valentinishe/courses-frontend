// import {} from "Types/Models/";
import { ModelLoaderType, LoaderType } from "Types/Models/tools/Loader";

export function isLoading(
  loaders: ModelLoaderType = [],
  loader: LoaderType
): boolean {
  return loaders.includes(loader);
}
