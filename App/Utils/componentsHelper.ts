export function trim({
  text,
  length,
  end
}: {
  text: string;
  length: number;
  end?: string;
}): string {
  if (text.length <= length) {
    return text;
  }
  return (
    String(text)
      .substring(0, length - 1)
      .trim() + String(end || "...")
  );
}
