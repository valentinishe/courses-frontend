export default {
  root: "/",
  home: "/home",
  notFound: "/404",
  registration: {
    user: {
      signin: "/registration/account",
      confirm: "/registration/confirm"
    }
  },
  auth: {
    user: {
      login: "/auth"
    }
  },
  dashboard: {
    courses: "/dashboard/courses",
    course: "/dashboard/course/:courseId",
    lesson: "/dashboard/course/:courseId/lesson/:lessonId"
  },
  user: {
    courses: "/courses",
    course: "/course/:courseId",
    lesson: "/course/:courseId/lesson/:lessonId"
  }
};
