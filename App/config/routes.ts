import App from "../App";

// Dasboard
import CoursesDashboardPage from "Views/Dashboard/Pages/Courses/index.tsx";
import CourseDashboardPage from "Views/Dashboard/Pages/Course/index.tsx";
import LessonDashboardPage from "Views/Dashboard/Pages/Lesson/index.tsx";

// Platform
import CoursesPage from "Views/Platform/Pages/Courses/index.tsx";
import CoursePage from "Views/Platform/Pages/Course/index.tsx";
import LessonPage from "Views/Platform/Pages/Lesson/index.tsx";
// import RegistrationPage from "Views/Platform/Pages/Registration";
import AuthPage from "Views/Platform/Pages/Auth";

import NotFoundPage from "Views/Core/Pages/NotFoundPage";

import routs from "./routs";

export default [
  {
    component: App,
    routes: [
      // Dashboard
      {
        path: routs.dashboard.courses,
        exact: true,
        component: CoursesDashboardPage
      },
      {
        path: routs.dashboard.course,
        exact: true,
        component: CourseDashboardPage
      },
      {
        path: routs.dashboard.lesson,
        exact: true,
        component: LessonDashboardPage
      },

      // Platform
      //   {
      //     path: routs.registration.user.signin,
      //     exact: true,
      //     component: RegistrationPage
      //   },
      {
        path: routs.auth.user.login,
        exact: true,
        component: AuthPage
      },
      {
        path: routs.user.courses,
        exact: true,
        component: CoursesPage
      },
      {
        path: routs.root,
        exact: true,
        component: CoursesPage
      },
      {
        path: routs.user.course,
        exact: true,
        component: CoursePage
      },
      {
        path: routs.user.lesson,
        exact: true,
        component: LessonPage
      },
      {
        path: routs.root,
        exact: false,
        component: NotFoundPage
      }
    ]
  }
];
