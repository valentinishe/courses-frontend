export default {
  API_URL: "http://127.0.0.1:1337",

  notificationConfig: {
    position: "tr",
    timer: 5
  },
  LOCALE: {
    EN: "en",
    UK: "uk",
    RU: "ru"
  }
};
