export default {
  required: "Поле обязательно!",
  minLength: "Трошки малувато, ще трошки! (мын - {min})",
  maxLength: "Ого! Та це багато, давай трошки менше! (макс. - {max})",
  rangeLength: "Нажаль треба, шоб довжина була в межах {min}>x>{max} =(",
  length: "Довжина повинна буть - {length}",

  isName: "Повинно мыстити лише украънську (ну и English)",
  isPasswordsMatching: "Паролы повинны буть однаковы!"
};
