import React from "react";
import { Container } from "semantic-ui-react";

// types
import { ModelBreadcrumbsType } from "Types/Models/tools/Breadcrumbs";

// components
import Menu from "Views/Core/Containers/Menu";
import Breadcrumbs from "Views/Core/Components/Breadcrumbs";

interface Props {
  children: any;
  sectionsBreadcrumbs?: ModelBreadcrumbsType[];
}

export default function(props: Props) {
  return (
    <Container style={{ margin: "15px 0" }}>
      <Menu />
      <Breadcrumbs sections={props.sectionsBreadcrumbs} />
      {props.children}
    </Container>
  );
}
