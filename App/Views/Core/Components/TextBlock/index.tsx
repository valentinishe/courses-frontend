import React from "react";
import { Segment, Container } from "semantic-ui-react";

interface Props {
  title: string;
  text: string;
}

const TextBlock = (props: Props) => (
  <Container textAlign="center" style={{ marginBottom: 50 }}>
    <h3>{props.title}</h3>
    <Segment raised textAlign="left">
      {props.text}
    </Segment>
  </Container>
);

export default TextBlock;
