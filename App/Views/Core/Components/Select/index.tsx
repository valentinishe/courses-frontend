   
import React from 'react';

type Props = {
    onChange: Function,
    options: {},
    fieldSelect: string,
    fieldName: string,
    value: string | number,
    name: string,
    error: string | false
}



export default (props) => (
    <div>
        <select 
            name={props.name}
            value={props.value}
            onChange={ (e) => props.onChange({ 
                name: props.name,
                value: e.target.value 
            })}
        >
            { props.options.map( (o, key) => (
                <option 
                    value={o[props.fieldSelect]}
                    key={key}
                >
                    { o[props.fieldName] }
                </option>
            ))}
        </select>
        <span>{props.error}</span>
    </div>
);