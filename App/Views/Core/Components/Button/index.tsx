import React from "react";
import { Button } from "semantic-ui-react";

interface Props {
  onClick: Function;
  text: string;
  compact?: boolean;
  color?: string;
  positive?: boolean;
  loading?: boolean;
  negative?: boolean;
  floated?: string;
}

export default props => {
  return (
    <Button
      onClick={props.onClick}
      color={props.color}
      loading={props.loading}
      compact={props.compact}
      positive={props.positive}
      negative={props.negative}
      floated={props.floated}
    >
      {props.text}
    </Button>
  );
};
