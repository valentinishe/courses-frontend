import React from "react";
// import * as ReactQuill from "react-quill"; // Typescript
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css"; // ES6

interface Props {
  onChange: Function;
  value: string;
  name: string;
  style?: any;
  // error: string | boolean;
}

interface State {
  value: string;
}

class WYSIWYG extends React.Component<Props, State> {
  modules = {
    toolbar: [
      [{ header: [1, 2, 3, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" }
      ],
      [{ align: [] }],
      [{ color: [] }, { background: [] }],
      [{ script: "sub" }, { script: "super" }],
      ["link", "image", "video"],
      ["clean"]
    ]
  };

  formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "video"
  ];

  handleChange = value => {
    this.props.onChange({ name: this.props.name, value });
  };

  render() {
    return (
      <ReactQuill
        style={{ minHeight: 200 }}
        value={this.props.value}
        onChange={this.handleChange}
        modules={this.modules}
        formats={this.formats}
      />
    );
  }
}

export default WYSIWYG;
