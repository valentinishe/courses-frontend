import React from "react";
import { ModelBreadcrumbsType } from "Types/Models/tools/Breadcrumbs";
import { Breadcrumb } from "semantic-ui-react";
import { Link } from "react-router-dom";

interface Props {
  sections?: ModelBreadcrumbsType[];
}

const BreadcrumbExampleProps = (props: Props) => {
  return props.sections ? (
    <Breadcrumb style={{ margin: "20px 0px 30px 0px" }}>
      {props.sections.map((b, k) => (
        <React.Fragment>
          {k > 0 && <Breadcrumb.Divider icon="left angle" />}
          {b.to ? (
            <Breadcrumb.Section link>
              <Link to={b.to || ""}>{b.content}</Link>
            </Breadcrumb.Section>
          ) : (
            <Breadcrumb.Section>{b.content}</Breadcrumb.Section>
          )}
        </React.Fragment>
      ))}
    </Breadcrumb>
  ) : null;
};

export default BreadcrumbExampleProps;
