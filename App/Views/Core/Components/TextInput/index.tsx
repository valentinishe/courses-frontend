import React from "react";
import { Input, Label, Popup } from "semantic-ui-react";

interface Props {
  onChange: Function;
  value: string | number;
  name: string;
  error: string | boolean;
  icon?: string;
  type?: string;
  iconPosition?: "left";
  errorPosition?: "right center";
}

const TextInput = (props: Props) => (
  <div style={{ display: "flex" }}>
    <Popup
      trigger={
        <Input
          name={props.name}
          value={props.value}
          icon={props.icon}
          iconPosition={props.iconPosition}
          type={props.type}
          onChange={e =>
            props.onChange({
              name: props.name,
              value: e.target.value
            })
          }
        />
      }
      open={Boolean(props.error)}
      content={props.error}
      hideOnScroll
      position={props.errorPosition}
      style={{ color: "red" }}
    />
  </div>
);

TextInput.defaultProps = {
  type: "text",
  icon: "",
  iconPosition: "right",
  errorPosition: "right center"
};

export default TextInput;
