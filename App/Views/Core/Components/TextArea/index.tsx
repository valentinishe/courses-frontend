import React from "react";
import { TextArea, Popup } from "semantic-ui-react";

interface Props {
  onChange: Function;
  value: string | number;
  name: string;
  style?: any;
  error: string | boolean;
  type?: string;
  autoHeight?: boolean;
  errorPosition?: "right center";
}

const TextInput = (props: Props) => (
  <div style={{ display: "flex", ...props.style }}>
    <Popup
      trigger={
        <TextArea
          name={props.name}
          value={props.value}
          type={props.type}
          autoHeight={props.autoHeight}
          onChange={(e: any) =>
            props.onChange({
              name: props.name,
              value: e.target.value
            })
          }
        />
      }
      open={Boolean(props.error)}
      content={props.error}
      hideOnScroll
      position={props.errorPosition}
      style={{ color: "red" }}
    />
  </div>
);

TextInput.defaultProps = {
  type: "text",
  errorPosition: "right center",
  autoHeight: false
};

export default TextInput;
