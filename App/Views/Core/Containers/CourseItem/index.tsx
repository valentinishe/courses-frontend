import React from "react";
import { Card, Icon, Image, Container } from "semantic-ui-react";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import routs from "config/routs";

// utils
import { trim } from "Utils/componentsHelper";

// types
import { ModelCourseType } from "Types/Models/Course";
import { ModelUserType } from "Types/Models/User";

// components
import defaultImage from "Public/images/defaultImage.png";

interface Props {
  push: typeof push;

  course: ModelCourseType;
  isDashboard: boolean;
  Me: ModelUserType;
}

const TITLE_LENGTH = 30;
const DESCRIPTION_LENGTH = 30;

const CourseItem = (props: Props) => (
  <Card centered>
    <Image
      src={props.course.logo || defaultImage}
      style={{
        backgroundColor: "white",
        height: 200,
        width: 300,
        cursor: "pointer"
      }}
      onClick={() =>
        props.push(
          routs.user.course.replace(":courseId", String(props.course.id))
        )
      }
    />
    <Card.Content
      style={{ cursor: "pointer" }}
      onClick={() =>
        props.push(
          routs.user.course.replace(":courseId", String(props.course.id))
        )
      }
    >
      <Card.Header style={{ height: 45 }}>
        {trim({ text: props.course.title, length: TITLE_LENGTH })}
      </Card.Header>
      <Card.Description style={{ height: 30 }}>
        {trim({ text: props.course.description, length: DESCRIPTION_LENGTH })}
      </Card.Description>
    </Card.Content>
    {props.isDashboard ? (
      <Card.Content extra>
        <Container textAlign="right">
          <a>
            <Icon
              style={{ cursor: "pointer" }}
              name="edit"
              onClick={() =>
                props.push(
                  routs.dashboard.course.replace(
                    ":courseId",
                    String(props.course.id)
                  )
                )
              }
            />
          </a>
        </Container>
      </Card.Content>
    ) : null}
  </Card>
);

const connector = connect(
  state => ({
    Me: state.User.me
  }),
  {
    push
  }
);

export default connector(CourseItem);
