import React from "react";
import { Button, Menu, Dropdown, Header, Icon } from "semantic-ui-react";
import routs from "config/routs";
import config from "config/config";
import { connect } from "react-redux";
import { get } from "lodash";
import jwt from "Services/JWT/JWT";
import { injectIntl } from "react-intl";

// actions
import { push } from "react-router-redux";
import { setMeInformationAction } from "Actions/User";
import { changeLocaleAction } from "Actions/tools/i18n";

// types
import { ModelUserType } from "Types/Models/User";
import { LocaleType } from "Types/Models/tools/i18n";

// utils
import { isDashboardLevel } from "Utils/authHelper";

interface Props {
  push: typeof push;
  setMeInformationAction: typeof setMeInformationAction;
  changeLocaleAction: typeof changeLocaleAction;

  Router: { pathname: string };
  Me: ModelUserType;
  Locale: LocaleType;
}

class MenuComponent extends React.Component<Props> {
  onChangeRoute = path => this.props.push(path);

  onLogOut = () => {
    jwt.setAccessToken("");
    jwt.setRefreshToken("");
    jwt.setExpireIn("");

    this.props.setMeInformationAction({ data: {} });
    this.props.push(routs.auth.user.login);
  };

  onChangeLocale = (e, { value }) => {
    this.props.changeLocaleAction({ locale: value });
  };

  isDashboardMenu = i18n => (
    <React.Fragment>
      <Menu.Item
        name={i18n.dashboard}
        active={this.props.Router.pathname === routs.dashboard.courses}
        onClick={() => this.onChangeRoute(routs.dashboard.courses)}
      />
      <Menu.Item
        name={i18n.courses}
        active={this.props.Router.pathname === routs.user.courses}
        onClick={() => this.onChangeRoute(routs.user.courses)}
      />
    </React.Fragment>
  );

  isUserMenu = i18n => (
    <Menu.Item
      name={i18n.courses}
      active={this.props.Router.pathname === routs.user.courses}
      onClick={() => this.onChangeRoute(routs.user.courses)}
    />
  );

  locales = [
    {
      key: config.LOCALE.UK,
      text: config.LOCALE.UK,
      value: config.LOCALE.UK,
      content: config.LOCALE.UK
    },
    {
      key: config.LOCALE.EN,
      text: config.LOCALE.EN,
      value: config.LOCALE.EN,
      content: config.LOCALE.EN
    },
    {
      key: config.LOCALE.RU,
      text: config.LOCALE.RU,
      value: config.LOCALE.RU,
      content: config.LOCALE.RU
    }
  ];

  render() {
    const i18n = get(this.props, "intl.messages.Menu", false);
    if (!i18n) {
      return null;
    }

    return (
      <Menu size="small">
        <Menu.Item>
          <img src="https://react.semantic-ui.com/logo.png" />
        </Menu.Item>
        {isDashboardLevel(this.props.Me.position)
          ? this.isDashboardMenu(i18n)
          : this.isUserMenu(i18n)}
        <Menu.Menu position="right">
          <Menu.Item>{get(this.props, "Me.name", "")}</Menu.Item>
          <Menu.Item>
            <Icon name="world" />
            <Header.Content>
              <Dropdown
                onChange={this.onChangeLocale}
                inline
                options={this.locales}
                defaultValue={this.props.Locale}
              />
            </Header.Content>
          </Menu.Item>
          <Menu.Item>
            <Button onClick={this.onLogOut}>{i18n.logOutBtn}</Button>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

const connector = connect(
  state => ({
    Me: state.User.me,
    Locale: state.I18N.locale,
    Router: state.router.location
  }),
  {
    push,
    setMeInformationAction,
    changeLocaleAction
  }
);

export default connector(injectIntl(MenuComponent));
