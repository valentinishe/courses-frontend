import React from "react";
import { Item, Icon, Container, Segment } from "semantic-ui-react";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import routs from "config/routs";

// utils
import { trim } from "Utils/componentsHelper";

// types
import { ModelLessonType } from "Types/Models/Lesson";
import { ModelCourseType } from "Types/Models/Course";
import { ModelUserType } from "Types/Models/User";

// components

interface Props {
  push: typeof push;

  lesson: ModelLessonType;
  isDashboard: boolean;
  courseId: ModelCourseType["id"];
  Me: ModelUserType;
}

const TITLE_LENGTH = 50;
const DESCRIPTION_LENGTH = 100;

const LessonItem = (props: Props) => {
  const onView = () => {
    props.push(
      routs.user.lesson
        .replace(":courseId", String(props.courseId))
        .replace(":lessonId", String(props.lesson.id))
    );
  };

  return (
    <Segment raised>
      <Item>
        <Item.Content>
          <Item.Header as="h3" style={{ cursor: "pointer" }} onClick={onView}>
            {trim({ text: props.lesson.title, length: TITLE_LENGTH })}
          </Item.Header>
          <Item.Description style={{ cursor: "pointer" }} onClick={onView}>
            {trim({
              text: props.lesson.description,
              length: DESCRIPTION_LENGTH
            })}
          </Item.Description>
          {props.isDashboard ? (
            <Item.Extra>
              <Container textAlign="right">
                <a>
                  <Icon
                    name="edit"
                    style={{ cursor: "pointer" }}
                    onClick={() =>
                      props.push(
                        routs.dashboard.lesson
                          .replace(":lessonId", String(props.lesson.id))
                          .replace(":courseId", String(props.courseId))
                      )
                    }
                  />
                </a>
              </Container>
            </Item.Extra>
          ) : null}
        </Item.Content>
      </Item>
    </Segment>
  );
};

const connector = connect(
  state => ({
    Me: state.User.me
  }),
  {
    push
  }
);

export default connector(LessonItem);
