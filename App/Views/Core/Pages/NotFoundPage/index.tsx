import React from "react";
import { Container } from "semantic-ui-react";
import { injectIntl } from "react-intl";
import { get } from "lodash";
import { connect } from "react-redux";
import routs from "config/routs";
import { push } from "react-router-redux";

// componetns
import Button from "Views/Core/Components/Button";

import NotFoundGif from "Public/gifs/travolta.gif";

interface Props {
  push: typeof push;
}

const NotFoundPage = (props: Props) => {
  console.log("props", props);
  const i18n = get(props, "intl.messages.NotFoundPage", false);
  if (!i18n) {
    return null;
  }

  return (
    <Container
      style={{
        display: "flex",
        flex: 1,
        height: "100vh",
        alignItems: "center",
        justifyContent: "space-between",
        padding: "100px 0",
        flexDirection: "column"
      }}
    >
      <img src={NotFoundGif} />
      <Button onClick={() => props.push(routs.root)} text={i18n.goRootBtn} />
    </Container>
  );
};

const connector = connect(
  () => ({}),
  { push }
);

export default connector(injectIntl(NotFoundPage));
