import React from "react";
import { Segment } from "semantic-ui-react";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import renderHTML from "react-render-html";

// utils
import { changeForm } from "Utils/formHelper";

// types
import { ModelContentType } from "Types/Models/Content";
import { ModelUserType } from "Types/Models/User";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// actions

// components
// import { isLoading } from "Utils/loaderHelpers";

interface Props {
  push: typeof push;
  onDelete: Function;
  onSave: Function;

  content: ModelContentType;
  Me: ModelUserType;
  Loaders: ModelLoaderType;
}

class ContentItem extends React.Component<Props> {
  loadersId = {
    save: "ContentItemSaveContent"
  };

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.content, nextProps.content)) {
      this.onChange({ name: "content", value: nextProps.content.data });
    }
  }

  onChange = changeForm.bind(this);

  render() {
    return <Segment raised>{renderHTML(this.props.content.data)}</Segment>;
  }
}

const connector = connect(
  state => ({
    Loaders: state.Loaders.loaders
  }),
  {
    push
  }
);

export default connector(ContentItem);
