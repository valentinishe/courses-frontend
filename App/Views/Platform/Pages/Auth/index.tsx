import * as React from "react";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import { Container, Segment, Form } from "semantic-ui-react";

// actions
import { postAuthAction } from "Actions/Auth";
import { clearErrorHandlerAction } from "Actions/tools/Errors";

// types
import { formItem } from "Types/FormHalpers";
import { ModelErrorType } from "Types/Models/tools/Errors";
import { ModelUserType } from "Types/Models/User";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// utils
import { changeForm } from "Utils/formHelper";
import { validationFields, errorToForm } from "Utils/validationHelper";
import { isLoading } from "Utils/loaderHelpers";

// components
import TextInput from "Views/Core/Components/TextInput";
import Button from "Views/Core/Components/Button";

import validationMap from "./validationMap";

interface State {
  form: {
    login: formItem<ModelUserType["login"]>;
    password: formItem<ModelUserType["password"]>;
  };
}

interface Props {
  Validations: ModelErrorType["validations"];
  ErrorMessage: ModelErrorType["message"];
  Loaders: ModelLoaderType;
  postAuthAction: Function;
  clearErrorHandlerAction: Function;
}

const styleContainer = {
  display: "flex",
  flex: "1",
  alignItems: "center",
  justifyContent: "center",
  height: "100vh"
};
class AuthPage extends React.Component<Props, State> {
  loaderId: string = "AuthPage";
  state = {
    form: {
      login: {
        value: "",
        error: false
      },
      password: {
        value: "",
        error: false
      }
    }
  };

  changeForm = changeForm.bind(this);
  validation = validationFields.bind(this, validationMap);
  errorToForm = errorToForm.bind(this);

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.Validations, nextProps.Validations)) {
      this.errorToForm(nextProps.Validations);
    }
    if (!isEqual(this.props.ErrorMessage, nextProps.ErrorMessage)) {
      alert(nextProps.ErrorMessage);
    }
  }

  signIn = () => {
    this.props.clearErrorHandlerAction({});
    this.validation(() => {
      this.props.postAuthAction({
        login: this.state.form.login.value,
        password: this.state.form.password.value,
        loaderId: this.loaderId
      });
    });
  };

  public render() {
    return (
      <div style={styleContainer}>
        <Segment compact>
          <Form>
            <Form.Field error={this.state.form.login.error}>
              <label>Логин</label>
              <TextInput
                name="login"
                value={this.state.form.login.value}
                error={this.state.form.login.error}
                onChange={this.changeForm}
                icon="phone"
                iconPosition="left"
              />
            </Form.Field>
            <Form.Field error={this.state.form.password.error}>
              <label>Пароль</label>
              <TextInput
                name="password"
                value={this.state.form.password.value}
                error={this.state.form.password.error}
                type="password"
                icon="lock"
                onChange={this.changeForm}
                iconPosition="left"
              />
            </Form.Field>
            <Container textAlign="right" fluid>
              <Button
                text="Гоу)"
                positive
                loading={isLoading(this.props.Loaders, this.loaderId)}
                onClick={this.signIn}
              />
            </Container>
          </Form>
        </Segment>
      </div>
    );
  }
}

const connector = connect(
  state => ({
    Validations: state.Errors.validations,
    ErrorMessage: state.Errors.message,
    Loaders: state.Loaders.loaders
  }),
  {
    postAuthAction,
    clearErrorHandlerAction
  }
);

export default connector(AuthPage);
