/* eslint-disable object-shorthand */

import { isRequired, isLength, isName } from "Utils/validationRules";

import constants from "config/validationConstants";
import messages from "config/validationMessages";

const validationMap = {
  login: {
    Methods: {
      required: isRequired,
      isLength: isLength(String(constants.minName), String(constants.maxName))
      // isName
    },
    Messages: {
      required: messages.required,
      isLength: messages.rangeLength
        .replace("{min}", String(constants.minName))
        .replace("{max}", String(constants.maxName))
      // isName: messages.isName
    }
  },
  password: {
    Methods: {
      required: isRequired,
      isLength: isLength(String(constants.minName), String(constants.maxName))
    },
    Messages: {
      required: messages.required,
      isLength: messages.rangeLength
        .replace("{min}", String(constants.minName))
        .replace("{max}", String(constants.maxName))
    }
  }
};

export default validationMap;
