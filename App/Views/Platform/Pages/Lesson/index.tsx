import React from "react";
import { get } from "lodash";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import { Container, Item } from "semantic-ui-react";
import routs from "config/routs";
import { injectIntl } from "react-intl";

// utlis
import { changeFormMulty } from "Utils/formHelper";
// import { isLoading } from "Utils/loaderHelpers";
import { trim } from "Utils/componentsHelper";

// types
import { formItem } from "Types/FormHalpers";
import { ModelContentType } from "Types/Models/Content";
import { ModelCourseType } from "Types/Models/Course";
import { ModelLessonType } from "Types/Models/Lesson";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// actions
import { getLessonAction } from "Actions/Lesson";
import { getCourseAction } from "Actions/Course";

// components
import TextBlock from "Views/Core/Components/TextBlock";
import DefaultLayout from "Views/Core/Layouts/defaultLayout";
import ContentItem from "Views/Platform/Containers/ContentItem";

interface Props {
  getLessonAction: typeof getLessonAction;
  getCourseAction: typeof getCourseAction;

  Lesson: ModelLessonType;
  Course: ModelCourseType;
  Contents: ModelContentType;
  match: any;
  Loaders: ModelLoaderType;
}
interface State {
  form: {
    title: formItem<ModelLessonType["title"]>;
    description: formItem<ModelLessonType["description"]>;
  };
}

class LessonPage extends React.Component<Props, State> {
  loadersId = {
    addNewContent: "addNewContent",
    saveContent: "saveContent",
    saveLesson: "saveLesson",
    loadLesson: "loadLesson",
    deleteContent: "deleteContent",
    deleteLesson: "deleteLesson",
    loadCourse: "loadCourse"
  };

  state = {
    form: {
      title: {
        value: "",
        error: false
      },
      description: {
        value: "",
        error: false
      }
    }
  };

  changeFormMulty = changeFormMulty.bind(this);

  componentDidMount() {
    this.props.getCourseAction({
      courseId: this.props.match.params.courseId,
      loaderId: this.loadersId.loadCourse
    });
    this.props.getLessonAction({
      lesson_id: this.props.match.params.lessonId,
      loaderId: this.loadersId.loadLesson
    });
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.Lesson, nextProps.Lesson)) {
      this.changeFormMulty({
        title: nextProps.Lesson.title,
        description: nextProps.Lesson.description
      });
    }
  }

  getSectionsBreadcrumbs = i18n => [
    {
      key: "courses",
      content: i18n.breadcrumbs.courses,
      to: routs.user.courses
    },
    {
      key: "course",
      content: trim({
        text: `${i18n.breadcrumbs.course} ${this.props.Course.title} `,
        length: 35
      }),
      to: routs.user.course.replace(":courseId", String(this.props.Course.id))
    },
    {
      key: "lesson",
      content: i18n.breadcrumbs.lesson
    }
  ];

  render() {
    const i18n = get(this.props, "intl.messages.LessonPage", false);
    if (!i18n) {
      return null;
    }
    return (
      <DefaultLayout sectionsBreadcrumbs={this.getSectionsBreadcrumbs(i18n)}>
        <TextBlock
          title={this.props.Lesson.title}
          text={this.props.Lesson.description}
        />

        <Container textAlign="center">
          <h3>{i18n.contents}</h3>
        </Container>
        <Item.Group divided style={{ margin: "50px 0" }}>
          {get(this.props, "Contents", []).map((c, key) => (
            <ContentItem content={c} key={key} />
          ))}
        </Item.Group>
      </DefaultLayout>
    );
  }
}

const connector = connect(
  state => ({
    Lesson: state.Lesson.lesson,
    Contents: state.Lesson.lesson.contents,
    Course: state.Course.course
  }),
  {
    getLessonAction,
    getCourseAction
  }
);

export default connector(injectIntl(LessonPage));
