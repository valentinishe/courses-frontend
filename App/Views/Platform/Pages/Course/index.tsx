import React from "react";
import { get } from "lodash";
import { connect } from "react-redux";
import routs from "config/routs";
import { Container, Item } from "semantic-ui-react";
import { injectIntl } from "react-intl";

// utils
// import { isLoading } from "Utils/loaderHelpers";

// types
import { ModelCourseType } from "Types/Models/Course";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// aactions
import { getCourseAction } from "Actions/Course";

// components
import DefaultLayout from "Views/Core/Layouts/defaultLayout";
import TextBlock from "Views/Core/Components/TextBlock";
import LessonItem from "Views/Core/Containers/LessonItem";

interface Props {
  getCourseAction: typeof getCourseAction;

  match: any;
  Course: ModelCourseType;
  Loaders: ModelLoaderType;
}

class CoursePage extends React.Component<Props> {
  loadersId = {
    saveCourse: "saveCourse",
    getCourse: "getCourse",
    deleteCourse: "deleteCourse",
    addLessons: "addLessons"
  };

  componentDidMount() {
    this.props.getCourseAction({
      courseId: this.props.match.params.courseId,
      loaderId: this.loadersId.getCourse
    });
  }

  getSectionsBreadcrumbs = i18n => [
    {
      key: "courses",
      content: i18n.breadcrumbs.courses,
      to: routs.user.courses
    },
    {
      key: "course",
      content: i18n.breadcrumbs.course
    }
  ];

  render() {
    const i18n = get(this.props, "intl.messages.CoursePage", false);
    if (!i18n) {
      return null;
    }
    return (
      <DefaultLayout sectionsBreadcrumbs={this.getSectionsBreadcrumbs(i18n)}>
        <TextBlock
          title={this.props.Course.title}
          text={this.props.Course.description}
        />

        <Container textAlign="center" style={{ marginTop: 40 }}>
          <h3>{i18n.lessons}</h3>
        </Container>
        <Item.Group divided style={{ margin: "50px 0" }}>
          {get(this.props, "Course.lessons", []).map((l, key) => (
            <LessonItem lesson={l} key={key} courseId={this.props.Course.id} />
          ))}
        </Item.Group>
      </DefaultLayout>
    );
  }
}

const connector = connect(
  state => ({
    Course: state.Course.course,
    Loaders: state.Loaders.loaders
  }),
  {
    getCourseAction
  }
);

export default connector(injectIntl(CoursePage));
