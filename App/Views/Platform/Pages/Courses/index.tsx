import React from "react";
// import { isLoading } from "Utils/loaderHelpers";
import { connect } from "react-redux";
import { Grid } from "semantic-ui-react";
// types
import { ModelCourseType } from "Types/Models/Course";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// actions
import { postCourseDashboardAction, getCoursesAction } from "Actions/Course";

// components
import DefaultLayout from "Views/Core/Layouts/defaultLayout";
import CourseItem from "Views/Core/Containers/CourseItem";

interface Props {
  getCoursesAction: typeof getCoursesAction;
  postCourseDashboardAction: typeof postCourseDashboardAction;

  Courses: ModelCourseType[];
  Loaders: ModelLoaderType;
}

class CoursesPage extends React.Component<Props> {
  loadersId = {
    getCourses: "getCourses",
    addCourse: "addCourse"
  };

  componentDidMount() {
    this.props.getCoursesAction({
      loaderId: this.loadersId.getCourses
    });
  }

  render() {
    return (
      <DefaultLayout>
        <Grid doubling stackable columns={4} style={{ marginTop: 30 }}>
          {this.props.Courses.map((course, key) => (
            <Grid.Column key={key}>
              <CourseItem course={course} />
            </Grid.Column>
          ))}
        </Grid>
      </DefaultLayout>
    );
  }
}

const connector = connect(
  state => ({
    Courses: state.Course.courses,
    Loaders: state.Loaders.loaders
  }),
  {
    postCourseDashboardAction,
    getCoursesAction
  }
);

export default connector(CoursesPage);
