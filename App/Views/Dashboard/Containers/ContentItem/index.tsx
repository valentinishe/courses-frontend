import React from "react";
import { Container, Segment } from "semantic-ui-react";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import { isEqual } from "lodash";

// utils
import { changeForm } from "Utils/formHelper";

// types
import { ModelContentType } from "Types/Models/Content";
import { ModelUserType } from "Types/Models/User";
import { formItem } from "Types/FormHalpers";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// actions

// components
import WYSIWYG from "Views/Core/Components/WYSIWYG";
import Button from "Views/Core/Components/Button";
import { isLoading } from "Utils/loaderHelpers";

interface State {
  form: {
    content: formItem<string>;
  };
}

interface Props {
  push: typeof push;
  onDelete: Function;
  onSave: Function;

  i18n: any;
  content: ModelContentType;
  Me: ModelUserType;
  Loaders: ModelLoaderType;
}

class ContentItem extends React.Component<Props, State> {
  loadersId = {
    save: "ContentItemSaveContent"
  };

  state = {
    form: {
      content: {
        value: this.props.content.data,
        error: false
      }
    }
  };

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.content, nextProps.content)) {
      this.onChange({ name: "content", value: nextProps.content.data });
    }
  }

  onChange = changeForm.bind(this);

  render() {
    return (
      <Segment raised>
        <WYSIWYG
          name="content"
          value={this.state.form.content.value}
          onChange={this.onChange}
        />
        <Container
          style={{
            marginTop: 20,
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <Button
            onClick={() => this.props.onDelete({ id: this.props.content.id })}
            negative
            text={this.props.i18n.deleteContentBtn}
            loading={isLoading(this.props.Loaders, this.loadersId.save)}
          />
          <Button
            onClick={() =>
              this.props.onSave({
                data: this.state.form.content.value,
                id: this.props.content.id
              })
            }
            positive
            text={this.props.i18n.saveBtn}
            loading={isLoading(this.props.Loaders, this.loadersId.save)}
          />
        </Container>
      </Segment>
    );
  }
}

const connector = connect(
  state => ({
    Loaders: state.Loaders.loaders
  }),
  {
    push
  }
);

export default connector(ContentItem);
