/* eslint-disable object-shorthand */

import {
  isRequired,
  isLength,
  isName
  // isPasswordsMatching
} from "Utils/validationRules";

import constants from "config/validationConstants";
import messages from "config/validationMessages";

const validationMap = {
  firstName: {
    Methods: {
      required: isRequired,
      isLength: isLength(constants.minName, constants.maxName),
      isName
    },
    Messages: {
      required: messages.required,
      isLength: messages.rangeLength
        .replace("{min}", String(constants.minName))
        .replace("{max}", String(constants.maxName)),
      isName: messages.isName
    }
  },
  lastName: {
    Methods: {
      required: isRequired,
      isLength: isLength(constants.minName, constants.maxName),
      isName
    },
    Messages: {
      required: messages.required,
      isLength: messages.rangeLength
        .replace("{min}", String(constants.minName))
        .replace("{max}", String(constants.maxName)),
      isName: messages.isName
    }
  },
  username: {
    Methods: {
      required: isRequired,
      isLength: isLength(constants.minName, constants.maxName),
      isName
    },
    Messages: {
      required: messages.required,
      isLength: messages.rangeLength
        .replace("{min}", String(constants.minName))
        .replace("{max}", String(constants.maxName)),
      isName: messages.isName
    }
  },
  phone: {
    Methods: {
      required: isRequired,
      isLength: isLength(constants.phone, constants.phone)
    },
    Messages: {
      required: messages.required,
      isLength: messages.length.replace("{length}", String(constants.phone))
    }
  },
  password: {
    Methods: {
      required: isRequired,
      isLength: isLength(constants.minName, constants.maxName)
    },
    Messages: {
      required: messages.required,
      isLength: messages.rangeLength
        .replace("{min}", String(constants.minName))
        .replace("{max}", String(constants.maxName))
    }
  },
  confirmPassword: {
    Methods: {
      required: isRequired
      // isPasswordsMatching
    },
    Messages: {
      required: messages.required
      // isPasswordsMatching: messages.isPasswordsMatching
    }
  }
};

export default validationMap;
