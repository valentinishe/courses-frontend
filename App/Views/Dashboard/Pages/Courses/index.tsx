import React from "react";
import routs from "config/routs";
import { isLoading } from "Utils/loaderHelpers";
import { connect } from "react-redux";
import { isEqual, get } from "lodash";
import { Grid, Container } from "semantic-ui-react";
import { injectIntl } from "react-intl";

// types
import { ModelCourseType } from "Types/Models/Course";
import { formItem } from "Types/FormHalpers";
import { ModelErrorType } from "Types/Models/tools/Errors";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// actions
import {
  postCourseDashboardAction,
  getCoursesDashboardAction
} from "Actions/Course";

// utils
import { changeForm } from "Utils/formHelper";
import { validationFields, errorToForm } from "Utils/validationHelper";

// components
import Button from "Views/Core/Components/Button";
import DefaultLayout from "Views/Core/Layouts/defaultLayout";
import CourseItem from "Views/Core/Containers/CourseItem";

import validationMapAddCourse from "./validationMap";

interface State {
  form: {
    title: formItem<ModelCourseType["title"]>;
    description: formItem<ModelCourseType["description"]>;
  };
}

interface Props {
  getCoursesDashboardAction: typeof getCoursesDashboardAction;
  postCourseDashboardAction: typeof postCourseDashboardAction;

  Validations: ModelErrorType["validations"];
  ErrorMessage: ModelErrorType["message"];
  Courses: ModelCourseType[];
  Loaders: ModelLoaderType;
}

class CoursesPage extends React.Component<Props, State> {
  loadersId = {
    getCourses: "getCourses",
    addCourse: "addCourse"
  };
  state = {
    form: {
      title: {
        value: "",
        error: false
      },
      description: {
        value: "",
        error: false
      }
    }
  };

  changeForm = changeForm.bind(this);
  validationAddCourse = validationFields.bind(this, validationMapAddCourse);
  errorToForm = errorToForm.bind(this);

  componentDidMount() {
    this.props.getCoursesDashboardAction({
      loaderId: this.loadersId.getCourses
    });
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.Validations, nextProps.Validations)) {
      this.errorToForm(nextProps.Validations);
    }
  }

  onAddCourse = i18n => {
    this.props.postCourseDashboardAction({
      title: i18n.newTitle,
      description: i18n.newDescription,
      loaderId: this.loadersId.addCourse
    });
  };

  render() {
    const i18n = get(this.props, "intl.messages.CoursesPage", false);
    if (!i18n) {
      return null;
    }

    return (
      <DefaultLayout>
        <Grid doubling stackable columns={4} style={{ marginTop: 30 }}>
          {this.props.Courses.map((course, key) => (
            <Grid.Column>
              <CourseItem isDashboard course={course} />
            </Grid.Column>
          ))}
        </Grid>
        <Container textAlign="center" style={{ margin: "100px 0" }}>
          <Button
            onClick={() => this.onAddCourse(i18n)}
            positive
            text={i18n.addCourseBtn}
            loading={isLoading(this.props.Loaders, this.loadersId.addCourse)}
          />
        </Container>
      </DefaultLayout>
    );
  }
}

const connector = connect(
  state => ({
    Validations: state.Errors.validations,
    Courses: state.Course.courses,
    Loaders: state.Loaders.loaders
  }),
  {
    postCourseDashboardAction,
    getCoursesDashboardAction
  }
);

export default connector(injectIntl(CoursesPage));
