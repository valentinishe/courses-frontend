import React from "react";
import { get } from "lodash";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import routs from "config/routs";
import { injectIntl } from "react-intl";
import { Form, Container, Divider, Item } from "semantic-ui-react";
// utils
import { changeForm, changeFormMulty } from "Utils/formHelper";
import { isLoading } from "Utils/loaderHelpers";
import { validationFields, errorToForm } from "Utils/validationHelper";
import { trim } from "Utils/componentsHelper";

// types
import { formItem } from "Types/FormHalpers";
import { ModelLessonType } from "Types/Models/Lesson";
import { ModelCourseType } from "Types/Models/Course";
import { ModelErrorType } from "Types/Models/tools/Errors";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// aactions
import {
  getCourseDashboardAction,
  deleteCourseDashboardAction,
  putCourseDashboardAction
} from "Actions/Course";
import { postLessonDashboardAction } from "Actions/Lesson";

// components
import DefaultLayout from "Views/Core/Layouts/defaultLayout";
import TextInput from "Views/Core/Components/TextInput";
import TextArea from "Views/Core/Components/TextArea";
import Button from "Views/Core/Components/Button";
import LessonItem from "Views/Core/Containers/LessonItem";

import validationMapAddCourse from "./validationMap";

interface State {
  form: {
    title: formItem<ModelLessonType["title"]>;
    description: formItem<ModelLessonType["description"]>;
  };
}

interface Props {
  getCourseDashboardAction: typeof getCourseDashboardAction;
  postLessonDashboardAction: typeof postLessonDashboardAction;
  deleteCourseDashboardAction: typeof deleteCourseDashboardAction;
  putCourseDashboardAction: typeof putCourseDashboardAction;

  Validations: ModelErrorType["validations"];
  ErrorMessage: ModelErrorType["message"];
  match: any;
  Course: ModelCourseType;
  Loaders: ModelLoaderType;
}

class CoursePage extends React.Component<Props, State> {
  loadersId = {
    saveCourse: "saveCourse",
    getCourse: "getCourse",
    deleteCourse: "deleteCourse",
    addLessons: "addLessons"
  };
  state = {
    form: {
      title: {
        value: "",
        error: false
      },
      description: {
        value: "",
        error: false
      }
    }
  };

  changeForm = changeForm.bind(this);
  changeFormMulty = changeFormMulty.bind(this);
  validationAddCourse = validationFields.bind(this, validationMapAddCourse);
  errorToForm = errorToForm.bind(this);

  componentDidMount() {
    this.props.getCourseDashboardAction({
      courseId: this.props.match.params.courseId,
      loaderId: this.loadersId.getCourse
    });
    if (this.props.Course.id) {
      this.changeFormMulty({
        title: this.props.Course.title,
        description: this.props.Course.description
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.Validations, nextProps.Validations)) {
      this.errorToForm(nextProps.Validations);
    }
    if (!isEqual(this.props.Course, nextProps.Course)) {
      this.changeFormMulty({
        title: nextProps.Course.title,
        description: nextProps.Course.description
      });
    }
  }

  getSectionsBreadcrumbs = i18n => [
    {
      key: "courses",
      content: i18n.breadcrumbs.courses,
      to: routs.dashboard.courses
    },
    {
      key: "course",
      content: i18n.breadcrumbs.course
    }
  ];

  onDeleteCourse = () => {
    this.props.deleteCourseDashboardAction({
      id: this.props.Course.id,
      loaderId: this.loadersId.deleteCourse
    });
  };

  onSaveCourse = () => {
    this.props.putCourseDashboardAction({
      title: this.state.form.title.value,
      description: this.state.form.description.value,
      id: this.props.Course.id,
      loaderId: this.loadersId.addLessons
    });
  };

  onAddLesson = i18n => {
    this.props.postLessonDashboardAction({
      title: i18n.newTitle,
      description: i18n.newDescription,
      course_id: this.props.Course.id,
      loaderId: this.loadersId.addLessons
    });
  };

  render() {
    const i18n = get(this.props, "intl.messages.CoursePage", false);
    if (!i18n) {
      return null;
    }

    return (
      <DefaultLayout sectionsBreadcrumbs={this.getSectionsBreadcrumbs(i18n)}>
        <Form style={{ marginBottom: 50 }}>
          <Form.Field error={this.state.form.title.error}>
            <label>{i18n.title}</label>
            <TextInput
              name="title"
              value={this.state.form.title.value}
              error={this.state.form.title.error}
              onChange={this.changeForm}
            />
          </Form.Field>
          <Form.Field error={this.state.form.description.error}>
            <label>{i18n.description}</label>
            <TextArea
              autoHeight
              style={{ minHeight: 200 }}
              name="description"
              value={this.state.form.description.value}
              error={this.state.form.description.error}
              type="description"
              onChange={this.changeForm}
            />
          </Form.Field>
          <Container
            style={{ display: "flex", justifyContent: "space-between" }}
            fluid
          >
            <Button
              text={i18n.deleteBtn}
              negative
              loading={isLoading(
                this.props.Loaders,
                this.loadersId.deleteCourse
              )}
              onClick={this.onDeleteCourse}
            />
            <Button
              text={i18n.saveBtn}
              positive
              loading={isLoading(this.props.Loaders, this.loadersId.saveCourse)}
              onClick={this.onSaveCourse}
            />
          </Container>
        </Form>

        <Divider inverted />

        <Container textAlign="center">
          <h3>{i18n.lessons}</h3>
        </Container>
        <Item.Group divided style={{ margin: "50px 0" }}>
          {get(this.props, "Course.lessons", []).map((l, key) => (
            <LessonItem
              isDashboard
              lesson={l}
              key={key}
              courseId={this.props.Course.id}
            />
          ))}
        </Item.Group>
        <Container textAlign="center" style={{ marginBottom: 100 }}>
          <Button
            onClick={() => this.onAddLesson(i18n)}
            positive
            text={i18n.addLessonBtn}
            loading={isLoading(this.props.Loaders, this.loadersId.addLessons)}
          />
        </Container>
      </DefaultLayout>
    );
  }
}

const connector = connect(
  state => ({
    Validations: state.Errors.validations,
    Course: state.Course.course,
    Loaders: state.Loaders.loaders
  }),
  {
    getCourseDashboardAction,
    postLessonDashboardAction,
    deleteCourseDashboardAction,
    putCourseDashboardAction
  }
);

export default connector(injectIntl(CoursePage));
