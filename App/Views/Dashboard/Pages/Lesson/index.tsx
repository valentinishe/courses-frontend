import React from "react";
import { get } from "lodash";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import { Form, Container, Divider, Item } from "semantic-ui-react";
import routs from "config/routs";
import { injectIntl } from "react-intl";

// utlis
import { changeForm, changeFormMulty } from "Utils/formHelper";
import { isLoading } from "Utils/loaderHelpers";
import { validationFields, errorToForm } from "Utils/validationHelper";
import { trim } from "Utils/componentsHelper";

// types
import { formItem } from "Types/FormHalpers";
import { ModelContentType } from "Types/Models/Content";
import { ModelCourseType } from "Types/Models/Course";
import { ModelLessonType } from "Types/Models/Lesson";
import { ModelLoaderType } from "Types/Models/tools/Loader";

// actions
import { ModelErrorType } from "Types/Models/tools/Errors";
import {
  postContentDashboardAction,
  deleteContentDashboardAction,
  putContentDashboardAction
} from "Actions/Content";
import {
  getLessonDashboardAction,
  putLessonDashboardAction,
  deleteLessonDashboardAction
} from "Actions/Lesson";

import { getCourseDashboardAction } from "Actions/Course";

// components
import TextInput from "Views/Core/Components/TextInput";
import TextArea from "Views/Core/Components/TextArea";
import Button from "Views/Core/Components/Button";
import DefaultLayout from "Views/Core/Layouts/defaultLayout";
import ContentItem from "Views/Dashboard/Containers/ContentItem";

import validationMapAddCourse from "./validationMap";

interface Props {
  getLessonDashboardAction: typeof getLessonDashboardAction;
  postContentDashboardAction: typeof postContentDashboardAction;
  deleteContentDashboardAction: typeof deleteContentDashboardAction;
  putContentDashboardAction: typeof putContentDashboardAction;
  putLessonDashboardAction: typeof putLessonDashboardAction;
  deleteLessonDashboardAction: typeof deleteLessonDashboardAction;
  getCourseDashboardAction: typeof getCourseDashboardAction;

  Lesson: ModelLessonType;
  Course: ModelCourseType;
  Contents: ModelContentType;
  Validations: ModelErrorType["validations"];
  ErrorMessage: ModelErrorType["message"];
  match: any;
  Loaders: ModelLoaderType;
}
interface State {
  form: {
    title: formItem<ModelLessonType["title"]>;
    description: formItem<ModelLessonType["description"]>;
  };
}

class LessonPage extends React.Component<Props, State> {
  loadersId = {
    addNewContent: "addNewContent",
    saveContent: "saveContent",
    saveLesson: "saveLesson",
    loadLesson: "loadLesson",
    deleteContent: "deleteContent",
    deleteLesson: "deleteLesson",
    loadCourse: "loadCourse"
  };

  state = {
    form: {
      title: {
        value: "",
        error: false
      },
      description: {
        value: "",
        error: false
      }
    }
  };

  changeForm = changeForm.bind(this);
  changeFormMulty = changeFormMulty.bind(this);
  validationAddCourse = validationFields.bind(this, validationMapAddCourse);
  errorToForm = errorToForm.bind(this);

  componentDidMount() {
    this.props.getCourseDashboardAction({
      courseId: this.props.match.params.courseId,
      loaderId: this.loadersId.loadCourse
    });
    this.props.getLessonDashboardAction({
      lesson_id: this.props.match.params.lessonId,
      loaderId: this.loadersId.loadLesson
    });
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props.Validations, nextProps.Validations)) {
      this.errorToForm(nextProps.Validations);
    }
    if (!isEqual(this.props.Lesson, nextProps.Lesson)) {
      this.changeFormMulty({
        title: nextProps.Lesson.title,
        description: nextProps.Lesson.description
      });
    }
  }

  getSectionsBreadcrumbs = i18n => [
    {
      key: "courses",
      content: i18n.breadcrumbs.courses,
      link: true,
      to: routs.dashboard.courses
    },
    {
      key: "course",
      content: trim({
        text: `${i18n.breadcrumbs.course} ${this.props.Course.title} `,
        length: 35
      }),
      link: true,
      to: routs.dashboard.course.replace(
        ":courseId",
        String(this.props.Course.id)
      )
    },
    {
      key: "lesson",
      content: i18n.breadcrumbs.lesson,
      active: true
    }
  ];

  onDeleteLesson = () => {
    this.props.deleteLessonDashboardAction({
      id: +this.props.Lesson.id,
      course_id: this.props.Lesson.course_id,
      loaderId: this.loadersId.deleteLesson
    });
  };

  onSaveLesson = () => {
    this.props.putLessonDashboardAction({
      title: this.state.form.title.value,
      description: this.state.form.description.value,
      id: this.props.Lesson.id,
      course_id: this.props.Lesson.course_id,
      loaderId: this.loadersId.saveLesson
    });
  };

  onDeleteContent = ({ id }) => {
    this.props.deleteContentDashboardAction({
      loaderId: this.loadersId.deleteContent,
      id
    });
  };

  onSaveContent = ({ data, id }) => {
    this.props.putContentDashboardAction({
      data,
      lesson_id: this.props.Lesson.id,
      loaderId: this.loadersId.saveContent,
      id
    });
  };
  onAddContent = i18n => {
    this.props.postContentDashboardAction({
      data: i18n.newContent,
      lesson_id: this.props.Lesson.id,
      loaderId: this.loadersId.addNewContent
    });
  };

  render() {
    const i18n = get(this.props, "intl.messages.LessonPage", false);
    if (!i18n) {
      return null;
    }

    return (
      <DefaultLayout sectionsBreadcrumbs={this.getSectionsBreadcrumbs(i18n)}>
        <Form style={{ marginBottom: 50 }}>
          <Form.Field error={this.state.form.title.error}>
            <label>{i18n.title}</label>
            <TextInput
              name="title"
              value={this.state.form.title.value}
              error={this.state.form.title.error}
              onChange={this.changeForm}
            />
          </Form.Field>
          <Form.Field error={this.state.form.description.error}>
            <label>{i18n.description}</label>
            <TextArea
              autoHeight
              style={{ minHeight: 200 }}
              name="description"
              value={this.state.form.description.value}
              error={this.state.form.description.error}
              type="description"
              onChange={this.changeForm}
            />
          </Form.Field>
          <Container
            style={{ display: "flex", justifyContent: "space-between" }}
            fluid
          >
            <Button
              text={i18n.deleteLessonBtn}
              negative
              loading={isLoading(
                this.props.Loaders,
                this.loadersId.deleteLesson
              )}
              onClick={this.onDeleteLesson}
            />
            <Button
              text={i18n.saveBtn}
              positive
              loading={isLoading(this.props.Loaders, this.loadersId.saveLesson)}
              onClick={this.onSaveLesson}
            />
          </Container>
        </Form>

        <Divider inverted />

        <Container textAlign="center">
          <h3>{i18n.contents}</h3>
        </Container>
        <Item.Group divided style={{ margin: "50px 0" }}>
          {get(this.props, "Contents", []).map((c, key) => (
            <ContentItem
              content={c}
              isDashboard
              key={key}
              i18n={i18n}
              onSave={this.onSaveContent}
              onDelete={this.onDeleteContent}
            />
          ))}
        </Item.Group>
        <Container textAlign="center" style={{ marginBottom: 100 }}>
          <Button
            onClick={() => this.onAddContent(i18n)}
            positive
            text={i18n.addContentBtn}
            loading={isLoading(
              this.props.Loaders,
              this.loadersId.addNewContent
            )}
          />
        </Container>
      </DefaultLayout>
    );
  }
}

const connector = connect(
  state => ({
    Validations: state.Errors.validations,
    Lesson: state.Lesson.lesson,
    Contents: state.Lesson.lesson.contents,
    Course: state.Course.course
  }),
  {
    getLessonDashboardAction,
    postContentDashboardAction,
    deleteContentDashboardAction,
    putContentDashboardAction,
    putLessonDashboardAction,
    deleteLessonDashboardAction,
    getCourseDashboardAction
  }
);

export default connector(injectIntl(LessonPage));
