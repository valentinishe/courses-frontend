import * as React from "react";
import { renderRoutes } from "react-router-config";
import { hot } from "react-hot-loader";
import { connect } from "react-redux";
import Notifications from "react-notification-system-redux";
import { IntlProvider, addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import ru from "react-intl/locale-data/ru";
import uk from "react-intl/locale-data/uk";

import i18n from "config/i18n.json";

addLocaleData([...en, ...ru, ...uk]);

const notificationStyle = {
  Containers: {
    DefaultStyle: {
      marginRight: 70
    }
  },
  NotificationItem: {
    DefaultStyle: {
      padding: 0,
      borderTop: "none",
      minHeight: 100,
      height: "auto",
      width: 370,
      borderRadius: 3,
      backgroundColor: "#fff"
    }
  },

  MessageWrapper: {
    DefaultStyle: {
      margin: 0,
      padding: 10,
      height: "auto"
    }
  },

  Title: {
    DefaultStyle: {
      height: 40,
      color: "#fff",
      display: "flex",
      alignItems: "center",
      paddingLeft: 25,
      margin: 0
    },

    success: {
      backgroundColor: "#21ba45"
    },
    error: {
      backgroundColor: "#db2828"
    }
  },
  Dismiss: {
    success: {
      backgroundColor: "#21ba45"
    },
    error: {
      backgroundColor: "#db2828"
    }
  }
};

const App = (props: any) => {
  return (
    <React.Fragment>
      <IntlProvider locale={props.Locale} messages={i18n[props.Locale]}>
        {renderRoutes(props.route.routes)}
      </IntlProvider>
      <Notifications
        notifications={props.notifications}
        style={notificationStyle}
      />
    </React.Fragment>
  );
};

const connector = connect(
  state => ({ notifications: state.notifications, Locale: state.I18N.locale }),
  {}
);

export default hot(module)(connector(App));
