import { combineReducers } from "redux";

// data
import Course from "./App/Course";
import Lesson from "./App/Lesson";
import User from "./App/User";

// tools
import { routerReducer } from "react-router-redux";
import Errors from "./tools/Errors";
import I18N from "./tools/i18n";
import Loaders from "./tools/Loader";
import { reducer as notifications } from "react-notification-system-redux";

const reducers = {
  Course,
  Lesson,
  User,

  // tools
  router: routerReducer,
  Errors,
  notifications,
  Loaders,
  I18N
};

export type ReducersType = typeof reducers;

export default combineReducers(reducers);
