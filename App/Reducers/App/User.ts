import { SET_ME_INFORMATION } from "Actions/User";

import { setMeInformation } from "Types/ReduxModels/User";

import { ModelLessonType } from "Types/Models/Lesson";

type StateType = {
  me: ModelLessonType | {};
};

type ActionType = setMeInformation;

const initState: StateType = {
  me: {}
};

export default (
  state: StateType = initState,
  action: ActionType
): StateType => {
  switch (action.type) {
    case SET_ME_INFORMATION:
      return {
        ...state,
        me: action.payload.data
      };
    default:
      return state;
  }
};
