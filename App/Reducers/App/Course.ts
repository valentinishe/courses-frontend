import { SET_COURSE, SET_COURSES } from "Actions/Course";

import { setCourseType, setCoursesType } from "Types/ReduxModels/Course";

import { ModelCourseType } from "Types/Models/Course";

type StateType = {
  course: ModelCourseType;
  courses: ModelCourseType[];
};

type ActionType = setCoursesType | setCourseType;

const initState: StateType = {
  courses: [],
  course: {
    title: "",
    description: "",
    id: 0,
    logo: ""
  }
};

export default (
  state: StateType = initState,
  action: ActionType
): StateType => {
  switch (action.type) {
    case SET_COURSES:
      return {
        ...state,
        courses: [...action.payload.courses]
      };
    case SET_COURSE:
      return {
        ...state,
        course:
          action.payload.course === null
            ? initState.course
            : action.payload.course
      };
    case SET_COURSES:
      return {
        ...state,
        courses:
          action.payload.courses !== undefined
            ? [...action.payload.courses]
            : state.courses
      };
    default:
      return state;
  }
};
