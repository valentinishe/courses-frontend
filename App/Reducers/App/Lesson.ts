import { SET_LESSON } from "Actions/Lesson";

import { setLessonType } from "Types/ReduxModels/Lesson";

import { ModelLessonType } from "Types/Models/Lesson";

type StateType = {
  lesson: ModelLessonType;
};

type ActionType = setLessonType;

const initState: StateType = {
  lesson: {
    id: 0,
    title: "",
    description: "",
    course_id: 0
  }
};

export default (
  state: StateType = initState,
  action: ActionType
): StateType => {
  switch (action.type) {
    case SET_LESSON:
      return {
        ...state,
        lesson: action.payload.lesson ? action.payload.lesson : initState.lesson
      };
    default:
      return state;
  }
};
