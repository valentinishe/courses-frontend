import { ADD_LOADER, DELETE_LOADER } from "Actions/tools/Loader";

import {
  addLoaderType,
  deleteLoaderType
} from "Types/ReduxModels/tools/Loader";

import { ModelLoaderType } from "Types/Models/tools/Loader";

type StateType = {
  loaders: ModelLoaderType;
};

type ActionType = addLoaderType | deleteLoaderType;

const initState: StateType = {
  loaders: []
};

export default (
  state: StateType = initState,
  action: ActionType
): StateType => {
  switch (action.type) {
    case ADD_LOADER:
      return {
        ...state,
        loaders: [...state.loaders, action.payload.loaderId]
      };
    case DELETE_LOADER:
      return {
        ...state,
        loaders: [...state.loaders.filter(l => l !== action.payload.loaderId)]
      };
    default:
      return state;
  }
};
