import config from "config/config";

import { SET_LOCALE } from "Actions/tools/i18n";

import { setLocaleType } from "Types/ReduxModels/tools/i18n";

import { LocaleType } from "Types/Models/tools/i18n";

type StateType = {
  locale: LocaleType;
};

type ActionType = setLocaleType;

const initState: StateType = {
  locale: localStorage.getItem("locale") || config.LOCALE.UK
};

export default (
  state: StateType = initState,
  action: ActionType
): StateType => {
  switch (action.type) {
    case SET_LOCALE:
      return {
        ...state,
        locale: action.payload.locale ? action.payload.locale : state.locale
      };
    default:
      return state;
  }
};
