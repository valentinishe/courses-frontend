import { SET_ERROR_HANDLER, CLEAR_ALL_ERRORS } from "Actions/tools/Errors";
import {
  setErrorHandlerType,
  clearErrorHandlerType
} from "Types/ReduxModels/tools/Errors";

import { ModelErrorType } from "Types/Models/tools/Errors";

interface State {
  validations: ModelErrorType["validations"];
  message: ModelErrorType["message"];
}

const initState: State = {
  validations: [],
  message: ""
};

type ActionsType = setErrorHandlerType | clearErrorHandlerType;

export default (state = initState, action: ActionsType): State => {
  switch (action.type) {
    case SET_ERROR_HANDLER:
      const newState = state;
      const { validations, message } = action.payload;
      if (validations) {
        newState.validations = validations;
      }
      if (message) {
        newState.message = message;
      }
      return {
        ...newState
      };
    case CLEAR_ALL_ERRORS:
      return {
        ...initState
      };
    default:
      return state;
  }
};
