import { fork, put } from "redux-saga/effects";
// import RegistrationUser from "Saga/App/Registration";
import { readyAction } from "Actions/Auth";

import Auth from "Saga/App/Auth";

//pages
import Course from "Saga/App/Course";
import Lesson from "Saga/App/Lesson";
import Content from "Saga/App/Content";
import User from "Saga/App/User";

// tools
import Errors from "Saga/tools/Errors";
import I18N from "Saga/tools/i18n";

export default function* rootSaga() {
  // yield fork(RegistrationUser);
  yield fork(Auth);

  // pages
  yield fork(Course);
  yield fork(Lesson);
  yield fork(Content);
  yield fork(User);

  // tools
  yield fork(Errors);
  yield fork(I18N);

  yield put(readyAction({}));
}
