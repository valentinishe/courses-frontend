import { takeEvery, put, call } from "redux-saga/effects";

import { CHANGE_LOCALE, setLocaleAction } from "Actions/tools/i18n";

import { changeLocaleType } from "Types/ReduxModels/tools/i18n";

export function* changeLocale({ payload }: changeLocaleType) {
  yield put(
    setLocaleAction({
      locale: payload.locale
    })
  );
  localStorage.setItem("locale", payload.locale);
}

export default function* rootSaga() {
  yield takeEvery(CHANGE_LOCALE, changeLocale);
}
