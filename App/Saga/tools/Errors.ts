import { takeEvery, put, call } from "redux-saga/effects";
// import { push } from "react-router-redux";
import constants from "config/config";
import * as notification from "react-notification-system-redux";

import {
  CALL_ERROR_HANDLER,
  setErrorHandlerAction
} from "Actions/tools/Errors";

import { callErrorHandlerType } from "Types/ReduxModels/tools/Errors";

export function* errorsHandler({ payload }: callErrorHandlerType) {
  if (!payload.error) {
    return;
  }

  switch (payload.code) {
    case 422:
      yield put(
        setErrorHandlerAction({
          validations: payload.validations
        })
      );
      break;

    case 401:
      return yield put(
        notification.error({
          title: "Памилка",
          message: payload.message,
          position: constants.notificationConfig.position,
          autoDismiss: constants.notificationConfig.timer
        })
      );
      break;

    default:
    // TODO: It's temporary. Before the release will remove!
    // yield put(errorNotification({
    //     title:   'Oops... :(',
    //     message: 'We have not had such a mistake yet. And it needs to be processed !!!! FRONT HELP'
    // }));
    // end
  }
  window.console.error("ПАМИЛКА", payload);
}

export default function* rootSaga() {
  yield takeEvery(CALL_ERROR_HANDLER, errorsHandler);
}
