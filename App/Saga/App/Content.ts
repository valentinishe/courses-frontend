import { takeEvery, put, call, select } from "redux-saga/effects";
import { push } from "react-router-redux";
import routs from "config/routs";
import { get } from "lodash";

// actions
import { callErrorHandlerAction } from "Actions/tools/Errors";
import { addLoaderAction, deleteLoaderAction } from "Actions/tools/Loader";
import {
  POST_CONTENT_DASHBOARD,
  DELETE_CONTENT_DASHBOARD,
  PUT_CONTENT_DASHBOARD
} from "Actions/Content";
import { setLessonAction } from "Actions/Lesson";

// types
import {
  postContentDashboardType,
  putContentDashboardType,
  deleteContentDashboardType
} from "Types/ReduxModels/Content";

import http from "Services/HTTP/HTTP";

export function* postContentDashboard({ payload }: postContentDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    data: payload.data,
    lesson_id: payload.lesson_id
  };

  try {
    const { body } = yield call(http.post, `/dashboard/content`, request);

    const { Lesson } = yield select(state => state);

    yield put(
      setLessonAction({
        lesson: {
          ...Lesson.lesson,
          contents: [...Lesson.lesson.contents, body]
        }
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* putContentDashboard({ payload }: putContentDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    data: payload.data,
    lesson_id: payload.lesson_id
  };

  try {
    const { body } = yield call(
      http.put,
      `/dashboard/content/${payload.id}`,
      request
    );

    const { Lesson } = yield select(state => state);

    yield put(
      setLessonAction({
        lesson: {
          ...Lesson.lesson,
          contents: [
            ...Lesson.lesson.contents.map(c => (+c.id !== +body.id ? c : body))
          ]
        }
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* deleteContentDashboard({
  payload
}: deleteContentDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    yield call(http.delete, `/dashboard/content/${payload.id}`);

    const { Lesson } = yield select(state => state);

    yield put(
      setLessonAction({
        lesson: {
          ...Lesson.lesson,
          contents: [
            ...Lesson.lesson.contents.filter(c => +c.id !== +payload.id)
          ]
        }
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export default function* rootSaga() {
  yield takeEvery(POST_CONTENT_DASHBOARD, postContentDashboard);
  yield takeEvery(PUT_CONTENT_DASHBOARD, putContentDashboard);
  yield takeEvery(DELETE_CONTENT_DASHBOARD, deleteContentDashboard);
}
