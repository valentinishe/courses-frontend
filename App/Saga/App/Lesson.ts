import { takeEvery, put, call, select } from "redux-saga/effects";
import { push } from "react-router-redux";
import routs from "config/routs";

// actions
import { callErrorHandlerAction } from "Actions/tools/Errors";
import { setCourseAction } from "Actions/Course";
import {
  POST_LESSON_DASHBOARD,
  PUT_LESSON_DASHBOARD,
  DELETE_LESSON_DASHBOARD,
  GET_LESSON_DASHBOARD,
  GET_LESSON,
  setLessonAction
} from "Actions/Lesson";
import {
  getLessonDashboardType,
  postLessonDashboardType,
  putLessonDashboardType,
  deleteLessonDashboardType,
  getLessonType
} from "Types/ReduxModels/Lesson";
import { addLoaderAction, deleteLoaderAction } from "Actions/tools/Loader";

import http from "Services/HTTP/HTTP";

// DASHBOARD

export function* postLessonDashboard({ payload }: postLessonDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    title: payload.title,
    description: payload.description,
    course_id: payload.course_id
  };

  try {
    const { body } = yield call(http.post, `/dashboard/lesson`, request);

    const { Course } = yield select(state => state);

    yield put(
      setCourseAction({
        course: {
          ...Course.course,
          lessons: [...Course.course.lessons, body]
        }
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* getLessonDashboard({ payload }: getLessonDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    const { body } = yield call(
      http.get,
      `/dashboard/lesson/${payload.lesson_id}`
    );

    yield put(
      setLessonAction({
        lesson: body
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* putLessonDashboard({ payload }: putLessonDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    title: payload.title,
    description: payload.description,
    course_id: payload.course_id
  };

  try {
    const { body } = yield call(
      http.put,
      `/dashboard/lesson/${payload.id}`,
      request
    );

    const { Lesson } = yield select(state => state);

    yield put(
      setLessonAction({
        lesson: {
          ...Lesson.lesson,
          title: body.title,
          description: body.description
        }
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* deleteLessonDashboard({ payload }: deleteLessonDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    yield call(http.delete, `/dashboard/lesson/${payload.id}`);

    yield put(
      setLessonAction({
        lesson: {}
      })
    );

    yield put(
      push(
        routs.dashboard.course.replace(":courseId", String(payload.course_id))
      )
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

// PLATFORM

export function* getLesson({ payload }: getLessonType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    const { body } = yield call(http.get, `/lesson/${payload.lesson_id}`);

    yield put(
      setLessonAction({
        lesson: body
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export default function* rootSaga() {
  // Dashboard
  yield takeEvery(POST_LESSON_DASHBOARD, postLessonDashboard);
  yield takeEvery(GET_LESSON_DASHBOARD, getLessonDashboard);
  yield takeEvery(PUT_LESSON_DASHBOARD, putLessonDashboard);
  yield takeEvery(DELETE_LESSON_DASHBOARD, deleteLessonDashboard);

  // Platform
  yield takeEvery(GET_LESSON, getLesson);
}
