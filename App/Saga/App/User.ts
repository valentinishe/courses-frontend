import { takeEvery, put, call } from "redux-saga/effects";

// actions
import { callErrorHandlerAction } from "Actions/tools/Errors";
import { GET_ME_INFORMATION, setMeInformationAction } from "Actions/User";
import { READY_ACTION } from "Actions/Auth";
import { addLoaderAction, deleteLoaderAction } from "Actions/tools/Loader";

// types
import { getMeInformation } from "Types/ReduxModels/User";

import http from "Services/HTTP/HTTP";

export function* getInformationAboutMe({ payload }: getMeInformation) {
  put(addLoaderAction({ loaderId: payload.loaderId }));
  try {
    const { body } = yield call(http.get, `/me`);
    yield put(setMeInformationAction({ data: body }));
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export default function* rootSaga() {
  yield takeEvery(GET_ME_INFORMATION, getInformationAboutMe);
  yield takeEvery(READY_ACTION, getInformationAboutMe);
}
