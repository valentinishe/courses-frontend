import { takeEvery, put, call, select } from "redux-saga/effects";
import { LOCATION_CHANGE, push } from "react-router-redux";
import routs from "config/routs";

// types
import { postAuthType } from "Types/ReduxModels/Auth";

// actions
import { callErrorHandlerAction } from "Actions/tools/Errors";
import { POST_AUTH } from "Actions/Auth";
import { getMeInformationAction } from "Actions/User";
import { addLoaderAction, deleteLoaderAction } from "Actions/tools/Loader";

// services
import http from "Services/HTTP/HTTP";
import jwt from "Services/JWT/JWT";

// utils
import { isUser } from "Utils/authHelper";

export function* auth({ payload }: postAuthType) {
  yield put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    login: payload.login,
    password: payload.password
  };

  try {
    const data = yield call(http.post, `/login`, request);

    jwt.setAccessToken(data.accessToken);
    jwt.setRefreshToken(data.refreshToken);
    jwt.setExpireIn(data.expiresIn);

    yield put(getMeInformationAction({ loaderId: "me" }));

    const { User } = yield select(state => state);

    if (isUser(User.me.position)) {
      return yield put(push(routs.user.courses));
    }
    return yield put(push(routs.dashboard.courses));
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    yield put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* permissions({ payload }) {
  const accessUrls = [routs.auth.user.login];
  if (
    (!jwt.hasAccessToken() || !jwt.hasRefreshToken()) &&
    !accessUrls.includes(payload.pathname)
  ) {
    jwt.setAccessToken("");
    jwt.setRefreshToken("");
    yield put(push(routs.auth.user.login));
  }

  if (payload.pathname === routs.root) {
    yield put(push(routs.user.courses));
  }
}

export default function* rootSaga() {
  yield takeEvery(POST_AUTH, auth);
  yield takeEvery(LOCATION_CHANGE, permissions);
}
