import { takeEvery, put, call, select } from "redux-saga/effects";
import { push } from "react-router-redux";
import routs from "config/routs";

// actions
import { callErrorHandlerAction } from "Actions/tools/Errors";
import {
  POST_COURSE_DASHBOARD,
  PUT_COURSE_DASHBOARD,
  GET_COURSES_DASHBOARD,
  GET_COURSE_DASHBOARD,
  DELETE_COURSE_DASHBOARD,
  GET_COURSES,
  GET_COURSE,
  setCoursesAction,
  setCourseAction
} from "Actions/Course";
import {
  postCourseDashboardType,
  putCourseDashboardType,
  getCourseDashboardType,
  getCoursesDashboardType,
  deleteCourseDashboardType,
  getCoursesType,
  getCourseType
} from "Types/ReduxModels/Course";
import { addLoaderAction, deleteLoaderAction } from "Actions/tools/Loader";

import http from "Services/HTTP/HTTP";
import jwt from "Services/JWT/JWT";

export function* getCoursesDashboard({ payload }: getCoursesDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    const { body } = yield call(http.get, `/dashboard/courses`);

    yield put(
      setCoursesAction({
        courses: body.data
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* getCourses({ payload }: getCoursesType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    const { body } = yield call(http.get, `/courses`);

    yield put(
      setCoursesAction({
        courses: body.data
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* postCourseDashboard({ payload }: postCourseDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    title: payload.title,
    description: payload.description
  };

  try {
    const { body } = yield call(http.post, `/dashboard/course`, request);

    const { Course } = yield select(state => state);

    yield put(
      setCoursesAction({
        courses: [...Course.courses, body]
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* putCourseDashboard({ payload }: putCourseDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  const request = {
    title: payload.title,
    description: payload.description
  };

  try {
    const { body } = yield call(
      http.put,
      `/dashboard/course/${payload.id}`,
      request
    );

    const { Course } = yield select(state => state);

    yield put(
      setCourseAction({
        course: {
          ...Course.course,
          ...body
        }
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* getCourseDashboard({ payload }: getCourseDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    const { body } = yield call(
      http.get,
      `/dashboard/course/${payload.courseId}`
    );

    yield put(
      setCourseAction({
        course: body
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export function* deleteCourseDashboard({ payload }: deleteCourseDashboardType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    yield call(http.delete, `/dashboard/course/${payload.id}`);

    const { Course } = yield select(state => state);

    yield put(
      setCourseAction({
        course: null
      })
    );

    yield put(
      setCoursesAction({
        courses: [...Course.courses.filter(c => +c.id !== +payload.id)]
      })
    );

    yield put(push(routs.dashboard.courses));
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

// PLATFORM

export function* getCourse({ payload }: getCourseType) {
  put(addLoaderAction({ loaderId: payload.loaderId }));

  try {
    const { body } = yield call(http.get, `/course/${payload.courseId}`);

    yield put(
      setCourseAction({
        course: body
      })
    );
  } catch (error) {
    yield put(callErrorHandlerAction({ ...error }));
  } finally {
    put(deleteLoaderAction({ loaderId: payload.loaderId }));
  }
}

export default function* rootSaga() {
  // Dashboard
  yield takeEvery(POST_COURSE_DASHBOARD, postCourseDashboard);
  yield takeEvery(GET_COURSES_DASHBOARD, getCoursesDashboard);
  yield takeEvery(GET_COURSE_DASHBOARD, getCourseDashboard);
  yield takeEvery(PUT_COURSE_DASHBOARD, putCourseDashboard);
  yield takeEvery(DELETE_COURSE_DASHBOARD, deleteCourseDashboard);
  //Platform
  yield takeEvery(GET_COURSES, getCourses);
  yield takeEvery(GET_COURSE, getCourse);
}
