/* eslint-disable max-len */

import { createAction } from "redux-actions";
import { ReduxAction } from "Types/ReduxHelpers";
import {
  postContentDashboardType,
  getContentDashboardType,
  setContentType,
  putContentDashboardType,
  deleteContentDashboardType
} from "Types/ReduxModels/Content";

export const POST_CONTENT_DASHBOARD: postContentDashboardType["type"] =
  "POST_CONTENT_DASHBOARD";
export const postContentDashboardAction: ReduxAction<
  postContentDashboardType["type"],
  postContentDashboardType["payload"]
> = createAction(POST_CONTENT_DASHBOARD);

export const PUT_CONTENT_DASHBOARD: putContentDashboardType["type"] =
  "PUT_CONTENT_DASHBOARD";
export const putContentDashboardAction: ReduxAction<
  putContentDashboardType["type"],
  putContentDashboardType["payload"]
> = createAction(PUT_CONTENT_DASHBOARD);

export const SET_CONTENT: setContentType["type"] = "SET_CONTENT";
export const setContentAction: ReduxAction<
  setContentType["type"],
  setContentType["payload"]
> = createAction(SET_CONTENT);

export const GET_CONTENT_DASHBOARD: getContentDashboardType["type"] =
  "GET_CONTENT_DASHBOARD";
export const getContentDashboardAction: ReduxAction<
  getContentDashboardType["type"],
  getContentDashboardType["payload"]
> = createAction(GET_CONTENT_DASHBOARD);

export const DELETE_CONTENT_DASHBOARD: deleteContentDashboardType["type"] =
  "DELETE_CONTENT_DASHBOARD";
export const deleteContentDashboardAction: ReduxAction<
  deleteContentDashboardType["type"],
  deleteContentDashboardType["payload"]
> = createAction(DELETE_CONTENT_DASHBOARD);
