import { createAction } from "redux-actions";
import { ReduxAction } from "Types/ReduxHelpers";
import { getMeInformation, setMeInformation } from "Types/ReduxModels/User";

export const GET_ME_INFORMATION: getMeInformation["type"] =
  "GET_ME_INFORMATION";
export const getMeInformationAction: ReduxAction<
  getMeInformation["type"],
  getMeInformation["payload"]
> = createAction(GET_ME_INFORMATION);

export const SET_ME_INFORMATION: setMeInformation["type"] =
  "SET_ME_INFORMATION";
export const setMeInformationAction: ReduxAction<
  setMeInformation["type"],
  setMeInformation["payload"]
> = createAction(SET_ME_INFORMATION);
