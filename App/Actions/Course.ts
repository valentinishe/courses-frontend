/* eslint-disable max-len */

import { createAction } from "redux-actions";
import { ReduxAction } from "Types/ReduxHelpers";
import {
  getCoursesDashboardType,
  getCoursesType,
  setCoursesType,
  postCourseDashboardType,
  putCourseDashboardType,
  deleteCourseDashboardType,
  setCourseType,
  getCourseDashboardType,
  getCourseType
} from "Types/ReduxModels/Course";

// Dashboard
export const GET_COURSES_DASHBOARD: getCoursesDashboardType["type"] =
  "GET_COURSES_DASHBOARD";
export const getCoursesDashboardAction: ReduxAction<
  getCoursesDashboardType["type"],
  getCoursesDashboardType["payload"]
> = createAction(GET_COURSES_DASHBOARD);

export const GET_COURSES: getCoursesType["type"] = "GET_COURSES";
export const getCoursesAction: ReduxAction<
  getCoursesType["type"],
  getCoursesType["payload"]
> = createAction(GET_COURSES);

export const POST_COURSE_DASHBOARD: postCourseDashboardType["type"] =
  "POST_COURSE_DASHBOARD";
export const postCourseDashboardAction: ReduxAction<
  postCourseDashboardType["type"],
  postCourseDashboardType["payload"]
> = createAction(POST_COURSE_DASHBOARD);

export const PUT_COURSE_DASHBOARD: putCourseDashboardType["type"] =
  "PUT_COURSE_DASHBOARD";
export const putCourseDashboardAction: ReduxAction<
  putCourseDashboardType["type"],
  putCourseDashboardType["payload"]
> = createAction(PUT_COURSE_DASHBOARD);

export const SET_COURSE: setCourseType["type"] = "SET_COURSE";
export const setCourseAction: ReduxAction<
  setCourseType["type"],
  setCourseType["payload"]
> = createAction(SET_COURSE);

export const GET_COURSE_DASHBOARD: getCourseDashboardType["type"] =
  "GET_COURSE_DASHBOARD";
export const getCourseDashboardAction: ReduxAction<
  getCourseDashboardType["type"],
  getCourseDashboardType["payload"]
> = createAction(GET_COURSE_DASHBOARD);

export const DELETE_COURSE_DASHBOARD: deleteCourseDashboardType["type"] =
  "DELETE_COURSE_DASHBOARD";
export const deleteCourseDashboardAction: ReduxAction<
  deleteCourseDashboardType["type"],
  deleteCourseDashboardType["payload"]
> = createAction(DELETE_COURSE_DASHBOARD);

// Platform

export const GET_COURSE: getCourseType["type"] = "GET_COURSE";
export const getCourseAction: ReduxAction<
  getCourseType["type"],
  getCourseType["payload"]
> = createAction(GET_COURSE);

export const SET_COURSES: setCoursesType["type"] = "SET_COURSES";
export const setCoursesAction: ReduxAction<
  setCoursesType["type"],
  setCoursesType["payload"]
> = createAction(SET_COURSES);
