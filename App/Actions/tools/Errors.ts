/* eslint-disable max-len */
   
import { createAction }              from 'redux-actions';
import  { ReduxAction }          from 'Types/ReduxHelpers';
import  {
    callErrorHandlerType,
    setErrorHandlerType,
    clearErrorHandlerType
} from 'Types/ReduxModels/tools/Errors';


export const CALL_ERROR_HANDLER:  callErrorHandlerType['type'] = 'CALL_ERROR_HANDLER';
export const callErrorHandlerAction: ReduxAction<
         callErrorHandlerType['type'],
         callErrorHandlerType['payload']
    > = createAction(CALL_ERROR_HANDLER);

export const SET_ERROR_HANDLER:  setErrorHandlerType['type'] = 'SET_ERROR_HANDLER';
export const setErrorHandlerAction: ReduxAction<
         setErrorHandlerType['type'],
         setErrorHandlerType['payload']
    > = createAction(SET_ERROR_HANDLER);

export const CLEAR_ALL_ERRORS:  clearErrorHandlerType['type'] = 'CLEAR_ALL_ERRORS';
export const clearErrorHandlerAction: ReduxAction<
         clearErrorHandlerType['type'],
         clearErrorHandlerType['payload']
    > = createAction(CLEAR_ALL_ERRORS);
