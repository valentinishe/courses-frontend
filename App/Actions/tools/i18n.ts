/* eslint-disable max-len */

import { createAction } from "redux-actions";
import { ReduxAction } from "Types/ReduxHelpers";
import { setLocaleType, changeLocaleType } from "Types/ReduxModels/tools/i18n";

export const SET_LOCALE: setLocaleType["type"] = "SET_LOCALE";
export const setLocaleAction: ReduxAction<
  setLocaleType["type"],
  setLocaleType["payload"]
> = createAction(SET_LOCALE);

export const CHANGE_LOCALE: changeLocaleType["type"] = "CHANGE_LOCALE";
export const changeLocaleAction: ReduxAction<
  changeLocaleType["type"],
  changeLocaleType["payload"]
> = createAction(CHANGE_LOCALE);
