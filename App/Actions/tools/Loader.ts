/* eslint-disable max-len */
   
import { createAction }              from 'redux-actions';
import  { ReduxAction }          from 'Types/ReduxHelpers';
import  {
    addLoaderType,
    deleteLoaderType
} from 'Types/ReduxModels/tools/Loader';


export const ADD_LOADER:  addLoaderType['type'] = 'ADD_LOADER';
export const addLoaderAction: ReduxAction<
         addLoaderType['type'],
         addLoaderType['payload']
    > = createAction(ADD_LOADER);


export const DELETE_LOADER:  deleteLoaderType['type'] = 'DELETE_LOADER';
export const deleteLoaderAction: ReduxAction<
         deleteLoaderType['type'],
         deleteLoaderType['payload']
    > = createAction(DELETE_LOADER);
