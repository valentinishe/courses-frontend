/* eslint-disable max-len */

import { createAction } from "redux-actions";
import { ReduxAction } from "Types/ReduxHelpers";
import { postAuthType, readyType } from "Types/ReduxModels/Auth";

export const POST_AUTH: postAuthType["type"] = "POST_AUTH";
export const postAuthAction: ReduxAction<
  postAuthType["type"],
  postAuthType["payload"]
> = createAction(POST_AUTH);

export const READY_ACTION: readyType["type"] = "READY_ACTION";
export const readyAction: ReduxAction<
  readyType["type"],
  readyType["payload"]
> = createAction(READY_ACTION);
