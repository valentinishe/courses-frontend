/* eslint-disable max-len */

import { createAction } from "redux-actions";
import { ReduxAction } from "Types/ReduxHelpers";
import {
  postLessonDashboardType,
  putLessonDashboardType,
  getLessonDashboardType,
  deleteLessonDashboardType,
  setLessonType,
  getLessonType
} from "Types/ReduxModels/Lesson";

// Dashboard
export const POST_LESSON_DASHBOARD: postLessonDashboardType["type"] =
  "POST_LESSON_DASHBOARD";
export const postLessonDashboardAction: ReduxAction<
  postLessonDashboardType["type"],
  postLessonDashboardType["payload"]
> = createAction(POST_LESSON_DASHBOARD);

export const PUT_LESSON_DASHBOARD: putLessonDashboardType["type"] =
  "PUT_LESSON_DASHBOARD";
export const putLessonDashboardAction: ReduxAction<
  putLessonDashboardType["type"],
  putLessonDashboardType["payload"]
> = createAction(PUT_LESSON_DASHBOARD);

export const SET_LESSON: setLessonType["type"] = "SET_LESSON";
export const setLessonAction: ReduxAction<
  setLessonType["type"],
  setLessonType["payload"]
> = createAction(SET_LESSON);

export const GET_LESSON_DASHBOARD: getLessonDashboardType["type"] =
  "GET_LESSON_DASHBOARD";
export const getLessonDashboardAction: ReduxAction<
  getLessonDashboardType["type"],
  getLessonDashboardType["payload"]
> = createAction(GET_LESSON_DASHBOARD);

export const DELETE_LESSON_DASHBOARD: deleteLessonDashboardType["type"] =
  "DELETE_LESSON_DASHBOARD";
export const deleteLessonDashboardAction: ReduxAction<
  deleteLessonDashboardType["type"],
  deleteLessonDashboardType["payload"]
> = createAction(DELETE_LESSON_DASHBOARD);

// Platform

export const GET_LESSON: getLessonType["type"] = "GET_LESSON";
export const getLessonAction: ReduxAction<
  getLessonType["type"],
  getLessonType["payload"]
> = createAction(GET_LESSON);
