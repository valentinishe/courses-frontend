import axios from "axios";
import config from "config/config";
import JWT from "Services/JWT/JWT";

class HTTP {
  // async refreshToken() {
  //     if (!this.jwtStorage.hasAccessToken() || !this.jwtStorage.isTokenExpired()) {
  //         return;
  //     }

  //     try {
  //         const response = await fetch(`${ this.configManager.get('apiUrl')  }/user/refresh`, {
  //             method:  'POST',
  //             headers: new Headers({ 'Content-Type': 'application/json' }),
  //             body:    JSON.stringify({
  //                 refresh_token: this.jwtStorage.getRefreshToken()
  //             })
  //         });

  //         const body:  IJWTStorage, "items"> = await response.json();

  //         // Update jwt storage
  //         this.jwtStorage
  //             .setAccessToken(body.access_token)
  //             .setExpiresIn(body.expires_in)
  //             .setRefreshToken(body.refresh_token)
  //             .setScope(body.scope)
  //             .setTokenType(body.token_type);
  //     } catch (err) {
  //         window.console.error(err);
  //     }
  // }

  static async get(url) {
    axios.defaults.headers.common["Authorization"] = JWT.getAccessToken();
    return new Promise(async (res, rej) => {
      const result = await axios.get(config.API_URL + url);

      if (result.data.error) {
        rej(result.data);
      } else {
        res(result.data);
      }
    });
  }

  static async post(url, data) {
    axios.defaults.headers.common["Authorization"] = JWT.getAccessToken();

    return new Promise(async (res, rej) => {
      const result = await axios.post(config.API_URL + url, data);

      if (result.data.error) {
        rej(result.data);
      } else {
        res(result.data);
      }
    });
  }

  static async put(url, data) {
    axios.defaults.headers.common["Authorization"] = JWT.getAccessToken();

    return new Promise(async (res, rej) => {
      const result = await axios.put(config.API_URL + url, data);

      if (result.data.error) {
        rej(result.data);
      } else {
        res(result.data);
      }
    });
  }

  static async delete(url) {
    axios.defaults.headers.common["Authorization"] = JWT.getAccessToken();

    return new Promise(async (res, rej) => {
      const result = await axios.delete(config.API_URL + url);

      if (result.data.error) {
        rej(result.data);
      } else {
        res(result.data);
      }
    });
  }
}

export default HTTP;
