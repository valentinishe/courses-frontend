const ACCESS_TOKEN = "ACCESS_TOKEN";
const REFRESH_TOKEN = "REFRESH_TOKEN";
const EXPIRE_IN_TOKEN = "EXPIRE_IN_TOKEN";

class JWT {
  constructor() {}

  static getAccessToken() {
    return localStorage.getItem(ACCESS_TOKEN) || false;
  }
  static getRefreshToken() {
    return localStorage.getItem(REFRESH_TOKEN) || false;
  }
  static getExpireInToken() {
    return localStorage.getItem(EXPIRE_IN_TOKEN) || false;
  }

  static hasAccessToken() {
    return !!this.getAccessToken();
  }
  static hasRefreshToken() {
    return !!this.getRefreshToken();
  }
  static hasExpireInToken() {
    return !!this.getExpireInToken();
  }

  static setAccessToken(token) {
    localStorage.setItem(ACCESS_TOKEN, token);
  }
  static setRefreshToken(token) {
    localStorage.setItem(REFRESH_TOKEN, token);
  }
  static setExpireIn(token) {
    localStorage.setItem(EXPIRE_IN_TOKEN, token);
  }
}

export default JWT;
