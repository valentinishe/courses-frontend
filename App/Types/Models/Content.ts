import { ModelLessonType } from "Types/Models/Lesson";

export interface ModelContentType {
  id: number;
  data: string;
  lesson_id: ModelLessonType["id"];
}
