export interface ModelUserType {
  id: number;
  name: string;
  login: string;
  password: string;
  atLastLogin: number;
  isDisabled: 0 | 1;
  position: 0 | 1 | 2;
}
