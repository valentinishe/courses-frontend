import { ModelCourseType } from "Types/Models/Course";

export interface ModelLessonType {
  id: number;
  title: string;
  description: string;
  course_id: ModelCourseType["id"];
}
