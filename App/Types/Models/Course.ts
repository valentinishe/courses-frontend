export interface ModelCourseType {
  id: number;
  title: string;
  logo: string;
  description: string;
}
