import { LOCALE } from "config/config";

export type LocaleType = LOCALE.EN | LOCALE.RU | LOCALE.UK;
