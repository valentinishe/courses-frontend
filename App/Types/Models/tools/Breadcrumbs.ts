export interface ModelBreadcrumbsType {
  key: string;
  content: string;
  to?: string;
}
