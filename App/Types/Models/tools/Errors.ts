export interface ModelErrorType {
  error: number;
  validations?: { [propName: string]: any };
  message?: string;
}
