export type LoaderType = string;
export type ModelLoaderType = LoaderType[];
