// ЧТО ЭТО?

export type FormErrorType = boolean | string;

export interface formItem<T> {
  value: T;
  error: FormErrorType;
}

// interface SpecType {
//     methods: {
//         [key: string]: (value: mixed) => FormErrorType
//     },
//     messages: {
//         [key: string]: string
//     }
// }

// interface ValidMapType {
//     [key: string]: SpecType
// }

// interface HandlerValueType {
//     name: string,
//     value: any
// }

// interface PasswordConfirmation {
//     password: string,
//     confirmPassword: string | boolean
// }

// interface NotificationType {
//     id: number,
//     title: string,
//     name: string,
//     checked: boolean
// }
