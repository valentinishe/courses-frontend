import { ModelLessonType } from "Types/Models/Lesson";

// Dashboard
export interface postLessonDashboardType {
  type: "POST_LESSON_DASHBOARD";
  payload: {
    title: ModelLessonType["title"];
    description: ModelLessonType["description"];
    course_id: ModelLessonType["course_id"];
    loaderId: string;
  };
}

export interface putLessonDashboardType {
  type: "PUT_LESSON_DASHBOARD";
  payload: {
    title: ModelLessonType["title"];
    description: ModelLessonType["description"];
    course_id: ModelLessonType["course_id"];
    id: ModelLessonType["id"];
    loaderId: string;
  };
}
export interface getLessonDashboardType {
  type: "GET_LESSON_DASHBOARD";
  payload: {
    lesson_id: ModelLessonType["id"];
    loaderId: string;
  };
}

export interface setLessonType {
  type: "SET_LESSON";
  payload: {
    lesson: ModelLessonType | {};
  };
}

export interface deleteLessonDashboardType {
  type: "DELETE_LESSON_DASHBOARD";
  payload: {
    id: ModelLessonType["id"];
    course_id: ModelLessonType["course_id"];
    loaderId: string;
  };
}

// Platform

export interface getLessonType {
  type: "GET_LESSON";
  payload: {
    lesson_id: ModelLessonType["id"];
    loaderId: string;
  };
}
