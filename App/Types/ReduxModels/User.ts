import { ModelUserType } from "Types/Models/User";
import { LoaderType } from "Types/Models/tools/Loader";

export interface getMeInformation {
  type: "GET_ME_INFORMATION";
  payload: {
    loaderId: LoaderType;
  };
}

export interface setMeInformation {
  type: "SET_ME_INFORMATION";
  payload: {
    data: ModelUserType | {};
  };
}
