import { ModelUserType } from "Types/Models/User";

export interface postAuthType {
  type: "POST_AUTH";
  payload: {
    login: ModelUserType["login"];
    password: ModelUserType["password"];
    loaderId: string;
  };
}

export interface readyType {
  type: "READY_ACTION";
  payload: {};
}
