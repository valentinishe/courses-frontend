   
import  { ModelUserType }       from 'Types/Models/User';


export interface postRegistrationUserType {
    type: 'POST_REGISTRATION_USER',
    payload: {
        name:   ModelUserType['name'],
        password:   ModelUserType['password'],    
        login:    ModelUserType['login']
    }
}
