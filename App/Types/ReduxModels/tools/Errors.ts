import { ModelErrorType } from "Types/Models/tools/Errors";

export interface callErrorHandlerType {
  type: "CALL_ERROR_HANDLER";
  payload: {
    error: boolean;
    code: ModelErrorType["error"];
    validations?: ModelErrorType["validations"];
    message: ModelErrorType["message"];
  };
}

export interface setErrorHandlerType {
  type: "SET_ERROR_HANDLER";
  payload: {
    validations?: ModelErrorType["validations"];
    message?: ModelErrorType["message"];
  };
}

export interface clearErrorHandlerType {
  type: "CLEAR_ALL_ERRORS";
  payload: {};
}
