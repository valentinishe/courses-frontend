import { LocaleType } from "Types/Models/tools/i18n";

export interface setLocaleType {
  type: "SET_LOCALE";
  payload: {
    locale: LocaleType;
  };
}

export interface changeLocaleType {
  type: "CHANGE_LOCALE";
  payload: {
    locale: LocaleType;
  };
}
