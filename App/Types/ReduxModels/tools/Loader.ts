   

export interface addLoaderType {
    type: "ADD_LOADER",
    payload:{
        loaderId: string
    }
}

export interface deleteLoaderType {
    type: "DELETE_LOADER",
    payload:{
        loaderId: string
    }
}
