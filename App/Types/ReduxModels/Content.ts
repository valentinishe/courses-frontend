import { ModelContentType } from "Types/Models/Content";

export interface postContentDashboardType {
  type: "POST_CONTENT_DASHBOARD";
  payload: {
    data: ModelContentType["data"];
    lesson_id: ModelContentType["lesson_id"];
    loaderId: string;
  };
}

export interface putContentDashboardType {
  type: "PUT_CONTENT_DASHBOARD";
  payload: {
    data: ModelContentType["data"];
    lesson_id: ModelContentType["lesson_id"];
    id: ModelContentType["id"];
    loaderId: string;
  };
}

export interface getContentDashboardType {
  type: "GET_CONTENT_DASHBOARD";
  payload: {
    id: ModelContentType["id"];
    loaderId: string;
  };
}

export interface setContentType {
  type: "SET_CONTENT";
  payload: {
    content: ModelContentType;
  };
}

export interface deleteContentDashboardType {
  type: "DELETE_CONTENT_DASHBOARD";
  payload: {
    id: ModelContentType["id"];
    loaderId: string;
  };
}
