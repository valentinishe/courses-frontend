import { ModelCourseType } from "Types/Models/Course";

// Dashboard
export interface getCoursesDashboardType {
  type: "GET_COURSES_DASHBOARD";
  payload: {
    loaderId: string;
  };
}

export interface setCoursesType {
  type: "SET_COURSES";
  payload: {
    courses: ModelCourseType[];
  };
}

export interface postCourseDashboardType {
  type: "POST_COURSE_DASHBOARD";
  payload: {
    title: ModelCourseType["title"];
    description: ModelCourseType["description"];
    loaderId: string;
  };
}

export interface putCourseDashboardType {
  type: "PUT_COURSE_DASHBOARD";
  payload: {
    title: ModelCourseType["title"];
    description: ModelCourseType["description"];
    id: ModelCourseType["id"];
    loaderId: string;
  };
}

export interface setCourseType {
  type: "SET_COURSE";
  payload: {
    course?: ModelCourseType;
  };
}

export interface setCoursesType {
  type: "SET_COURSES";
  payload: {
    courses: ModelCourseType[];
  };
}

export interface getCourseDashboardType {
  type: "GET_COURSE_DASHBOARD";
  payload: {
    courseId: ModelCourseType;
    loaderId: string;
  };
}
export interface deleteCourseDashboardType {
  type: "DELETE_COURSE_DASHBOARD";
  payload: {
    id: ModelCourseType["id"];
    loaderId: string;
  };
}

// Platform
export interface getCourseType {
  type: "GET_COURSE";
  payload: {
    courseId: ModelCourseType;
    loaderId: string;
  };
}

export interface getCoursesType {
  type: "GET_COURSES";
  payload: {
    loaderId: string;
  };
}
