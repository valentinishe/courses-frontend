import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware                      from 'redux-saga';
import { routerMiddleware }                      from 'react-router-redux';

export default function configureStore(rootReducer, rootSaga, history, initialState) {
    const sagaMiddleware   = createSagaMiddleware();
    const composeEnhancers =
              process.env.NODE_ENV !== 'production' &&
              window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
                  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
                      name:             'MyApp', actionsBlacklist: [ 'REDUX_STORAGE_SAVE' ]
                  }) : compose;

    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(sagaMiddleware, routerMiddleware(history)))
    );

    sagaMiddleware.run(rootSaga);

    return store;
}
