import "babel-polyfill";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { renderRoutes } from "react-router-config";
import { ConnectedRouter } from "react-router-redux";
import configureStore from "./App/Store/configureStore";
import History from "./App/History";
import routes from "./App/config/routes";
import rootReducer from "./App/Reducers";
import rootSaga from "./App/Saga";

export const store = configureStore(rootReducer, rootSaga, History);

class ContainerApp extends React.Component {
  componentDidCatch(error, info) {
    console.error("APP ERROR");
    console.error("error", error);
    console.error("info", info);
  }

  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={History}>
          {renderRoutes(routes)}
        </ConnectedRouter>
      </Provider>
    );
  }
}

ReactDOM.render(<ContainerApp />, document.getElementById("root"));
